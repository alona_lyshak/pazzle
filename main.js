var zIndex = 1
var host = 'https://www.icalendar37.net/v/external_resources/'

function crearDreta(i) {
    var path = [
        {
            draw: "q -4,20 -5,43 l-5,0 a11,11 1,1,0 0,14 l5,0 q1,20 5,43",
            invers: "q -4,-20  -5,-43 l-5,0 a11,11 1,1,1 0,-14 l5,0 q 1-20 5,-43"
        },
        {
            draw: "q -4,20 -5,43 l5,0 a11,11 1,1,1 0,14 l-5,0 q1,20 5,43",
            invers: "q -4,-20 -5,-43 l 5,0 a11,11 1,1,0 0,-14 l-5,0 q 1-20 5,-43"
        },
        {
            draw: "q 4,20 5,43 l-5,0 a11,11 1,1,0 0,14 l5,0 q -1,20 -5,43",
            invers: "q 4,-20  5,-43 l-5,0 a11,11 1,1,1 0,-14 l5,0 q -1-20 -5,-43"
        },
        {
            draw: "q 4,20 5,43 l5,0 a11,11 1,1,1 0,14 l-5,0	q -1,20 -5,43",
            invers: "q 4,-20  5,-43 l5,0 a11,11 1,1,0 0,-14 l-5,0 q -1-20 -5,-43"
        },
    ]

    return path[Math.floor(Math.random() * path.length)]

}
function crearInferior(i) {
    var path = [
        {
            draw: "q -20,-4 -43,-5 l0,-5 a11,11 1,1,0 -14,0 l0,5 q-20,1 -43,5",
            invers: "q 20,-4 43,-5 l0,-5 a11,11 1,1,1 14,0 l0,5 q20,1 43,5 "
        },
        {
            draw: "q -20,-4 -43,-5 l0,5 a11,11 1,1,1 -14,0 l0,-5 q-20,1 -43,5",
            invers: "q 20,-4 43,-5 l0,5 a11,11 1,1,0 14,0 l0,-5 q20,1 43,5 "
        },
        {
            draw: "q -20,4 -43,5 l0,-5 a11,11 1,1,0 -14,0 l0,5 q-20,-1 -43,-5",
            invers: "q 20,4 43,5 l0,-5 a11,11 1,1,1 14,0 l0,5 q20,-1 43,-5 "
        },
        {
            draw: "q -20,4 -43,5 l0,5 a11,11 1,1,1 -14,0 l0,-5 q-20,-1 -43,-5",
            invers: "q 20,4 43,5 l0,5 a11,11 1,1,0 14,0 l0,-5 q20,-1 43,-5 "
        },
    ]

    return path[Math.floor(Math.random() * path.length)]
}
function inversDreta(i) {
    return { draw: director[i - 1].b.invers }
}
function inversSuperior(i) {
    return { draw: director[i - numCols].c.invers }
}

function dadesPeza(row, col, index) {
    var object = {}
    switch (true) {
        case (col == 0 && row == 0):
            object = {
                type: "supEsquerra",
                a: { draw: " l 100,0 " },
                b: crearDreta(index),
                c: crearInferior(index),
                d: { draw: " l 0,-100" },
            }
            break;
        case (row == 0 && col == numCols - 1):
            object = {
                type: "supDreta",
                a: { draw: " l 100,0 " },
                b: { draw: " l 0,100 " },
                c: crearInferior(index),
                d: inversDreta(index),
            }
            break;
        case (row == numRows - 1 && col == 0):
            object = {
                type: "infEsquerra",
                a: inversSuperior(index),
                b: crearDreta(index),
                c: { draw: " l -100,0 " },
                d: { draw: " l 0,-100 " },
            }
            break;
        case (row == numRows - 1 && col == numCols - 1):
            object = {
                type: "infDreta",
                a: inversSuperior(index),
                b: { draw: " l 0,100 " },
                c: { draw: " l -100,0 " },
                d: inversDreta(index),
            }
            break;
        case (row == 0):
            object = {
                type: "superior",
                a: { draw: " l 100,0 " },
                b: crearDreta(index),
                c: crearInferior(index),
                d: inversDreta(index),
            }
            break;
        case (col == 0):
            object = {
                type: "esquerra",
                a: inversSuperior(index),
                b: crearDreta(index),
                c: crearInferior(index),
                d: { draw: " l 0,-100 " },
            }
            break;
        case (col == numCols - 1):
            object = {
                type: "dreta",
                a: inversSuperior(index),
                b: { draw: " l 0,100 " },
                c: crearInferior(index),
                d: inversDreta(index),
            }
            break;
        case (row == numRows - 1):
            object = {
                type: "inferior",
                a: inversSuperior(index),
                b: crearDreta(index),
                c: { draw: " l -100,0 " },
                d: inversDreta(index),
            }
            break;
        default:
            object = {
                type: "central",
                a: inversSuperior(index),
                b: crearDreta(index),
                c: crearInferior(index),
                d: inversDreta(index),
            }
    }
    object.col = col
    object.row = row
    object.index = index
    object.path = function () { return this.a.draw + this.b.draw + this.c.draw + this.d.draw + "z" }
    return object

}


function createSvg(pathPeza, x, y, index) {
    xmlns = "http://www.w3.org/2000/svg"
    var svg = document.createElementNS(xmlns, "svg")
    var idPattern = "row" + x + "col" + y
    var pattern = '<defs><pattern id="' + idPattern + '" patternUnits="userSpaceOnUse" width="600" height="400"><image xlink:href="' + host + model.urlImg + '" x="' + (x * -100 + 40) + '" y="' + (y * -100 + 40) + '" width="' + (numCols * 100) + '" height="' + (numRows * 100) + '" /></pattern></defs>'
    svg.innerHTML = pattern
    svg.setAttribute("width", 180)
    svg.setAttribute("height", 180)
    var path = document.createElementNS(xmlns, "path")
    path.setAttribute("d", "M40,40 " + pathPeza)
    path.setAttribute("fill", "url(#" + idPattern + ")")
    path.style.fill = "url(#" + idPattern + ")"
    svg.appendChild(path)
    var move = document.createElement("DIV")
    move.appendChild(svg)
    move.className = "move"
    move.path = path
    move.angle = 0
    move.occupy = false
    move.position = function () { return { left: this.offsetLeft + 50, top: this.offsetTop + 50 } }
    move.index = index
    var position = document.createElement("DIV")
    position.className = "position"
    position.index = index
    position.occupied = false
    document.querySelector("#container").appendChild(position)
    position.appendChild(move)
    move.style.zIndex = zIndex++
    move.zIndexPrevi = move.style.zIndex


}
var i, element
var models = [

    {
        numRows: 4,
        numCols: 6,
        urlImg: "4x6.jpg"
    },
    {
        numRows: 2,
        numCols: 2,
        urlImg: "2x2.jpg"
    },
    {
        numRows: 3,
        numCols: 3,
        urlImg: "3x3.png"
    },
    {
        numRows: 4,
        numCols: 4,
        urlImg: "4x4.jpg"
    },


    {
        numRows: 5,
        numCols: 5,
        urlImg: "5x5.png"
    },
    {
        numRows: 5,
        numCols: 7,
        urlImg: "7x5.jpg"
    },
    {
        numRows: 6,
        numCols: 8,
        urlImg: "8x6.jpg"
    },

]

function createPuzzle(model) {
    document.getElementById("container").innerHTML = ""
    director = []
    index = 0
    numRows = model.numRows
    numCols = model.numCols
    document.getElementById("container").style.width = numCols * 100 + "px"
    document.getElementById("container").style.backgroundPositionY = 100 * model.numRows + "px"
    for (row = 0; row < numRows; row++) {
        for (col = 0; col < numCols; col++) {
            var dades = dadesPeza(row, col, index)
            director.push(dades)
            createSvg(dades.path(), col, row, index)
            index++
        }
    }
}

document.addEventListener("DOMContentLoaded", function () {
    model = models[2]
    createPuzzle(model)
})

